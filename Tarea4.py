# Jorge Anibal Chuncho Garrochamba
# jorge.a.chuncho@unl.edu.ec
# Tarea 4
import pygame
#inicia pygame
pygame.init()
#medidas ventana
ancho = 900
largo = 700
#colores
Azuloscuro = 0,0, 128
azul = 0,0,225
verde = 0,225,0
Grante = 128,0,0
Turquesa = 0,128,128
"""

"""
import math

class Punto:
    x= 0
    y= 0

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            return "I cuandrante"
        elif self.x < 0 and self.y > 0:
            return "II cuandrante"
        elif self.x < 0 and self.y < 0:
            return "III cuadrante"
        elif self.x > 0 and self.y < 0:
             return "IV cuadrante"
        elif self.x != 0 and self.y == 0:
             return "{} se sitúa sobre el eje X"
        elif self.x == 0 and self.y != 0:
            return "{} se sitúa sobre el eje Y"
        else:
             return "sobre el origen"
        #return self.x and self.y

    def vector(self, p):
        v= Punto(p.x - self.x, p.y - self.y)
        return v

    def distancia(self, p):
        d = math.sqrt( (p.x - self.x)**2 + (p.y - self.y)**2 )
        return d

    def __str__(self):
        return f"({self.x}, {self.y})"


class Rectángulo:
    punto_inicial= None
    punto_final= None

    def __init__(self, punto_inicial, punto_final):
        self.punto_inicial = punto_inicial
        self.punto_final = punto_final

    def base(self):
        return self.punto_final.x - self.punto_inicial.x

    def altura(self):
        return self.punto_final.y - self.punto_inicial.y

    def área(self):
        return  self.base()*self.altura()


if __name__ == '__main__':


    A = Punto(2,3)
    B = Punto(5,5)
    C = Punto(-3, -1)
    D = Punto(0,0)

    print (f"El punto {A} se encuentra en el: {A.cuadrante()}")
    print (f"El punto {B} se encuentra en el: {A.cuadrante()}")
    print (f"El punto {C} se encuentra en el: {C.cuadrante()}")
    print (f"El punto {D} se encuentra en el: {D.cuadrante()}")

    print (f"La distancia entre el punto {A} y {B} es {A.distancia(B)}")
    print (f"La distancia entre el punto {B} y {A} es {B.distancia(A)}")

    da= A.distancia(D)
    db= B.distancia(D)
    dc= C.distancia(D)
    if da > db and da > dc:
        print (f"El punto {A} se encuentra más lejos del origen")
    elif db > da and db > dc:
        print(f"El punto {B} se encuentra más lejos del origen")
    else:
        print(f"El punto {C} se encuentra más lejos del origen")

    rec = Rectángulo(A, B)
    print("La base del rectángulo es {}".format(rec.base()))
    print("La altura del rectángulo es {}".format(rec.altura()))
    print("El área del rectángulo es {}".format(rec.área ()))

    # Cambio el título de la ventana
    pygame.display.set_caption(" RECTANGULO")
    # ventana
    ventana = pygame.display.set_mode((ancho, largo))
    # dibujo
    ventana.fill(Azuloscuro)
    # dibujo rectangulo con el modulo draw
    pygame.draw.rect(ventana, verde, (2,3, 480, 640))

    # actualizar
    pygame.display.update()
    # cerrar ventana
    running = True
    while running:
        for event in pygame.event.get():
            # evento para cerrar la ventana
            if event.type == pygame.QUIT:
                running = False

    # salir
    pygame.quit()
